django==3.2.13
djangorestframework==3.13.1
psycopg2-binary==2.9.3
django-environ==0.8.1
gunicorn==20.1.0
whitenoise==6.1.0